/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Lateefah
 * Created: Nov 3, 2016
 */

create table user (
    email varchar(50) not null,
    first_name varchar(50) not null,
    last_name varchar(50) not null,
    phone_number varchar(20) not null,
    password varchar(255) not null,
    creation_date date not null,
    primary key (email)
)engine=innodb default charset=utf8;

create table role (
    id int not null,
    role varchar(50) not null,
    primary key (id)
)engine=innodb default charset=utf8;

create table privilege (
    id int not null,
    privilege varchar(50) not null,
    primary key (id)
)engine=innodb default charset=utf8;

create table user_role (
    email varchar(50) not null,
    role_id int not null,
    primary key (email, role_id),
    foreign key(email) references user(email) on update cascade on delete no action,
    foreign key(role_id) references role(id) on update cascade on delete no action
)engine=innodb default charset=utf8;

create table role_privilege (
    role_id int not null,
    privilege_id int not null,
    primary key (role_id, privilege_id),
    foreign key(role_id) references role(id) on update cascade on delete no action,
    foreign key(privilege_id) references privilege(id) on update cascade on delete no action
)engine=innodb default charset=utf8;