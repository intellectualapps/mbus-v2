/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.affinislabs.pus.data.manager;

import com.affinislabs.pus.data.provider.DataProviderLocal;
import com.affinislabs.pus.model.User;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.Stateless;

/**
 *
 * @author Lateefah
 */
@Stateless
public class UserDataManager implements UserDataManagerLocal {

    @EJB
    private DataProviderLocal crud;    

    @Override
    public User create(User user) throws EJBTransactionRolledbackException {
        return crud.create(user);
    }

    @Override
    public User update(User user) {
        return crud.update(user);
    }

    @Override
    public User get(String userId) {
        return crud.find(userId, User.class);
    }

    @Override
    public void delete(User user) {
        crud.delete(user);
    }

    @Override
    public List<User> getAllUsers() {
        return crud.findAll(User.class);
    }    

}

