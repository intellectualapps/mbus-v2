/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.affinislabs.pus.util.exception;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 *
 * @author buls
 */
@Provider
public class DuplicateUserKeyExceptionMapper implements ExceptionMapper<DuplicateUserKeyException> {  

    @Override
    public Response toResponse(DuplicateUserKeyException duke) {
        return Response.status(duke.getStatus())
                .entity(new ErrorMessage(duke))
                .type(MediaType.APPLICATION_JSON).
                build();
    }
    
}
