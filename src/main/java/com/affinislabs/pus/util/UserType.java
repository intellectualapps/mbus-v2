/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.affinislabs.pus.util;

/**
 *
 * @author buls
 */
public enum UserType {

    USER("us"), MENTOR("mn"), 
    ADMIN("ad");
    
    String description;

    UserType(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return description;
    }

    
}
