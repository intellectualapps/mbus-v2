/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.affinislabs.pus.publisher;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.Singleton;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;
import javax.jms.DeliveryMode;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.Topic;
import javax.jms.TopicConnection;
import javax.jms.TopicConnectionFactory;
import javax.jms.TopicSession;
import javax.naming.Context;
import javax.naming.InitialContext;
import com.affinislabs.pus.model.User;

/**
 * This class publishes new user created events to the new user topic.
 * Topic connection and Topic need to be configured in Glassfish for this work
 * @author buls
 */

@Singleton
public class NewUserPublisher {

    @Resource
    TimerService timerService;
    private User newUser = null;

    private Logger logger = Logger.getLogger(
            "NewUserPublisher");

    public void publishNewUserEvent(User newUser, long timeout) {
        logger.info(NewUserPublisher.class.getName() + 
                "Publishing new user created event for: " + newUser.getEmail());

        this.newUser = newUser;
        Timer timer = timerService.createSingleActionTimer(timeout,
                new TimerConfig());
    }

    @Timeout
    private void prepareBroadcastEvent(Timer timer) {
        try {
            String message = new ObjectMapper().writeValueAsString(newUser);
            broadcastEvent(message);
        } catch (Exception e) {
            logger.severe(NewUserPublisher.class.getName() + 
                    " Failed to prepare new user broadcast: " + e.getMessage());
        }
    }

    private void broadcastEvent(String message) {
        logger.info(NewUserPublisher.class.getName() + 
                " Broadcasting new user created event: " + message);

        TopicConnection topicConnection = null;
        try {

            Context initialContext = new InitialContext();
            TopicConnectionFactory qcf = (TopicConnectionFactory) initialContext.lookup("jms/cccon");
            topicConnection = qcf.createTopicConnection();
            TopicSession topicSession = topicConnection.createTopicSession(false, 
                    Session.AUTO_ACKNOWLEDGE);

            TextMessage textMessage = topicSession.createTextMessage();
            textMessage.setJMSDeliveryMode(DeliveryMode.PERSISTENT);
            textMessage.setText(message);

            Topic newUserCreatedTopic = (Topic) initialContext.lookup("jms/newUserTopic");
            MessageProducer sender = topicSession.createProducer(newUserCreatedTopic);

            sender.send(textMessage);

            logger.info(NewUserPublisher.class.getName() + 
                    " New user created event published with data: " + message);
        } catch (Exception e) {
            logger.severe(NewUserPublisher.class.getName() + 
                    " Error occured trying to broadcast new user event: " + e.getMessage());
        } finally {
            if (topicConnection != null) {
                try {
                    topicConnection.close();
                } catch (Exception e) {
                    logger.severe(NewUserPublisher.class.getName() + 
                            " Failed to close topic connection: " + e.getMessage());
                }
            }
        }
    }

}
