/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.affinislabs.pus.publisher;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;
import javax.jms.DeliveryMode;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.Topic;
import javax.jms.TopicConnection;
import javax.jms.TopicConnectionFactory;
import javax.jms.TopicSession;
import javax.naming.Context;
import javax.naming.InitialContext;
import com.affinislabs.pus.model.User;
import com.affinislabs.pus.model.UserPasswordResetToken;

/**
 *
 * @author Lateefah
 */
@Stateless
public class ResetPasswordPublisher {
    @Resource
    TimerService timerService;
    private UserPasswordResetToken token = null;

    private Logger logger = Logger.getLogger(ResetPasswordPublisher.class.getName());

    public void publishResetPasswordEvent(UserPasswordResetToken token, long timeout) {
        logger.info(ResetPasswordPublisher.class.getName() + 
                "Publishing reset event for: " + token.getEmail());

        this.token = token;
        Timer timer = timerService.createSingleActionTimer(timeout,
                new TimerConfig());
    }
    
    @Timeout
    private void prepareBroadcastEvent(Timer timer) {
        try {
            String message = new ObjectMapper().writeValueAsString(token);
            broadcastEvent(message);
        } catch (Exception e) {
            logger.severe(ResetPasswordPublisher.class.getName() + 
                    " Failed to prepare reset password broadcast: " + e.getMessage());
        }
    }
    
    private void broadcastEvent(String message) {
        logger.info(ResetPasswordPublisher.class.getName() + 
                " Broadcasting reset password event: " + message);

        TopicConnection topicConnection = null;
        try {

            Context initialContext = new InitialContext();
            TopicConnectionFactory qcf = (TopicConnectionFactory) initialContext.lookup("jms/cccon");
            topicConnection = qcf.createTopicConnection();
            TopicSession topicSession = topicConnection.createTopicSession(false, 
                    Session.AUTO_ACKNOWLEDGE);

            TextMessage textMessage = topicSession.createTextMessage();
            textMessage.setJMSDeliveryMode(DeliveryMode.PERSISTENT);
            textMessage.setText(message);

            Topic newUserCreatedTopic = (Topic) initialContext.lookup("jms/resetPasswordTopic");
            MessageProducer sender = topicSession.createProducer(newUserCreatedTopic);

            sender.send(textMessage);

            logger.info(ResetPasswordPublisher.class.getName() + 
                    " reset password event published with data: " + message);
        } catch (Exception e) {
            logger.severe(ResetPasswordPublisher.class.getName() + 
                    " Error occured trying to broadcast reset password event: " + e.getMessage());
        } finally {
            if (topicConnection != null) {
                try {
                    topicConnection.close();
                } catch (Exception e) {
                    logger.severe(ResetPasswordPublisher.class.getName() + 
                            " Failed to close topic connection: " + e.getMessage());
                }
            }
        }
    }
}
