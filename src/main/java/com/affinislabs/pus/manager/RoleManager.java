/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.affinislabs.pus.manager;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.List;
import com.affinislabs.pus.data.manager.PrivilegeDataManagerLocal;
import com.affinislabs.pus.data.manager.RoleDataManagerLocal;
import com.affinislabs.pus.model.Privilege;
import com.affinislabs.pus.model.Role;
import com.affinislabs.pus.model.RolePrivilege;
import com.affinislabs.pus.model.RolePrivilegePK;

/**
 *
 * @author buls
 */
@Stateless
public class RoleManager implements RoleManagerLocal{
        
    @EJB
    private RoleDataManagerLocal roleDataManager;
    private PrivilegeDataManagerLocal privilegeDataManager;    
    
    @Override
    public Role getRole(String roleId) {
        return roleDataManager.get(roleId);
    }
    
    @Override
    public List<Role> getAllRoles() {               
        return roleDataManager.getAllRoles();        
    }
        
    
    @Override
    public List<RolePrivilege> getRolePrivileges(String roleId) {
        return roleDataManager.getByRole(roleId);
    }
    
    @Override
    public Role addRole(String roleId, String description) {        
        Role appRole = null; 
        
        if (roleId != null && description != null) {
            Role role = new Role();
            
            role.setRoleId(roleId);
            role.setDescription(description);
                        
            return roleDataManager.create(role);                        
        } 
        return appRole;
    }
   
    @Override
    public Boolean deleteRole(String roleId) {
        if (roleId != null){
            Role role = roleDataManager.get(roleId);
                       
            roleDataManager.delete(role);
            return true;
        }
        return false;    
    }
    
    @Override
    public Role updateRole(String roleId, String description) {                                
        if (roleId != null && description != null) {
            Role role = roleDataManager.get(roleId);
            
            role.setDescription(description);
            
            return roleDataManager.update(role);            
        } 
        return null;
    }
    
    @Override
    public Boolean addRolePrivilege(String roleId, String privilegeId){
        if(roleId != null && privilegeId != null){   
            Role role = new Role();
            role.setRoleId(roleId);
            
            Privilege privilege = new Privilege();
            privilege.setPrivilegeId(privilegeId);
            
            RolePrivilege rolePrivilege = new RolePrivilege();
            rolePrivilege.setRole(role);
            rolePrivilege.setPrivilege(privilege);
            
            RolePrivilegePK rolePrivilegePK = new RolePrivilegePK();
            rolePrivilegePK.setPrivilegeId(privilegeId);
            rolePrivilegePK.setRoleId(roleId);
            rolePrivilege.setRoleAndPrivilegePK(rolePrivilegePK);
                                 
            roleDataManager.addRolePrivilege(rolePrivilege);
            return true;            
        }
        return false;   
    }
    
    @Override
    public Boolean deleteRolePrivilege(String roleId, String privilegeId){
        if(roleId != null && privilegeId != null){
            List<RolePrivilege> fetchedRolePrivileges = roleDataManager.getRoleAndPrivilege(roleId, privilegeId);            
            for (RolePrivilege rolePrivilege : fetchedRolePrivileges){
                roleDataManager.deleteRolePrivilege(rolePrivilege); 
            }                      
            return true;     
        }
        return false;
    }
    
}
