/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.affinislabs.pus.manager;

import java.util.ArrayList;
import com.affinislabs.pus.data.manager.UserDataManagerLocal;
import com.affinislabs.pus.model.User;
import com.affinislabs.pus.pojo.AppUser;
import com.affinislabs.pus.util.MD5;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.ejb.EJB;
import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.Stateless;
import javax.ws.rs.core.Response;
import com.affinislabs.pus.data.manager.UserPasswordResetTokenDataManagerLocal;
import com.affinislabs.pus.data.manager.UserRoleDataManagerLocal;
import com.affinislabs.pus.model.Privilege;
import com.affinislabs.pus.model.Role;
import com.affinislabs.pus.model.UserPasswordResetToken;
import com.affinislabs.pus.model.UserRole;
import com.affinislabs.pus.model.UserRolePK;
import com.affinislabs.pus.pojo.AppPrivilege;
import com.affinislabs.pus.pojo.AppRole;
import com.affinislabs.pus.pojo.PlatformCallback;
import com.affinislabs.pus.publisher.NewUserPublisher;
import com.affinislabs.pus.publisher.ResetPasswordPublisher;
import com.affinislabs.pus.publisher.ResendEmailVerificationPublisher;
import com.affinislabs.pus.publisher.ResendSmsVerificationPublisher;
import com.affinislabs.pus.util.CodeGenerator;
import com.affinislabs.pus.util.JWT;
import com.affinislabs.pus.util.JikaDecode;
import com.affinislabs.pus.util.UserType;
import com.affinislabs.pus.util.exception.DuplicateUserKeyException;
import com.affinislabs.pus.util.exception.InternalException;
import com.affinislabs.pus.util.exception.InvalidEmailFormatException;
import com.affinislabs.pus.util.exception.InvalidUserTypeException;
import com.affinislabs.pus.util.exception.NullUserAttributeException;
import com.affinislabs.pus.util.exception.UserDoesNotExistException;
import com.affinislabs.pus.util.exception.InvalidLoginCredentialsException;

/**
 *
 * @author Lateefah
 */
@Stateless
public class UserManager implements UserManagerLocal {

    @EJB
    private UserDataManagerLocal userDataManager;
    @EJB
    private NewUserPublisher newUserPublisher;
    @EJB
    private ResetPasswordPublisher resetPasswordPublisher;
    @EJB
    private ResendSmsVerificationPublisher resendSmsVerificationPublisher;
    @EJB
    private ResendEmailVerificationPublisher resendEmailVerificationPublisher;
    @EJB
    private CodeGenerator codeGenerator;
    @EJB
    private UserRoleDataManagerLocal userRoleDataManager;
    @EJB
    private UserPasswordResetTokenManagerLocal userPasswordResetTokenManager;

    private final String INCOMPLETE_DATA_ERROR = "Incomplete data provided";
    private final String INCOMPLETE_DATA_ERROR_DETAILS = "One or more attributes of"
            + " the user are null or empty";
    private final String INVALID_USER_TYPE_ERROR = "Invalid user type";
    private final String INVALID_USER_TYPE_ERROR_DETAILS = "An unknown user type value specified for user-type";
    private final String USER_NOT_FOUND_ERROR = "User not found";
    private final String USER_NOT_FOUND_ERROR_DETAILS = "The user with the supplied identifier does not exist";
    private final String USER_ALREADY_EXISTS_ERROR = "User already exists";
    private final String USER_ALREADY_EXISTS_ERROR_DETAILS = "A user already exists with the supplied identifier";
    private final String INTERNAL_SERVER_ERROR = "Internal server error";
    private final String INTERNAL_SERVER_ERROR_DETAILS = "An internal server error occured";
    private final String ADD_USER_LINK = "/user";
    private final String VERIFY_USER_EMAIL_LINK = "/user/email";
    private final String AUTHENTICATE_USER_LINK = "/user/authenticate";
    private final String VERIFY_USER_SMS_CODE_LINK = "/{email}/phone/code/{sms-code}";
    private final String RESEND_USER_SMS_CODE_LINK = "/{email}/phone/code";
    private final String RESEND_USER_EMAIL_CODE_LINK = "/{email}/email/link";
    private final String PASSWORD_RESET_TOKEN_LINK = "/password-token";
    private final long PUBLISH_TIMEOUT = 1000;
    private final String USER_CALLBACK = "http://user.pentor.com/email-verified";
    private final String MENTOR_CALLBACK = "http://mentor.pentor.com/email-verified";
    private final String ADMIN_CALLBACK = "http://pentor.com/email-verified";
    private final String NO_ROLE_CALLBACK = "http://ridealo.ng/error";
    private final String INVALID_LOGIN_CREDENTIALS_ERROR = "Invalid login credentials";
    private final String INVALID_LOGIN_CREDENTIALS_ERROR_DETAILS = "Invalid Username/Password combination";
    private final String INVALID_EMAIL_FORMAT_ERROR = "Invalid email";
    private final String INVALID_EMAIL_FORMAT_ERROR_DETAILS = "Invalid email format";    
    private final String RESET_USER_PASSWORD_LINK = "/user/password";
    private final long TOKEN_LIFETIME= 86400;
    private final String GET_USER_DETAILS = "/user/{email}";

    @Override
    public AppUser getAppUser(String email) {
        AppUser appUser = new AppUser();
        appUser.setEmail(email);
        appUser.setUserRoles(getUserAppRoles(email));
        return appUser;
    }

    @Override
    public AppUser getAppUser(User user) {
        AppUser appUser = new AppUser();
        appUser.setEmail(user.getEmail());
        appUser.setFirstName(user.getFirstName());
        appUser.setLastName(user.getLastName());
        appUser.setPhoneNumber(user.getPhoneNumber());
        appUser.setCreationDate(user.getCreationDate());
        appUser.setSmsVerificationCode(user.getSmsVerificationCode());
        appUser.setIsEmailAddressVerified(user.isEmailAddressVerified());
        return appUser;
    }
    
    @Override
    public AppRole getAppRole(UserRole userRole) {
        AppRole appRole = new AppRole();
        appRole.setRoleId(userRole.getRole().getRoleId());
        appRole.setDescription(userRole.getRole().getDescription());        
        return appRole;
    }       

    @Override
    public AppUser authenticateUser (String email, String password) 
            throws NullUserAttributeException, InvalidEmailFormatException, InvalidLoginCredentialsException {        
        
        if (email == null || email.isEmpty() || password == null || password.isEmpty()) {
            throwNullUserAttributeException(AUTHENTICATE_USER_LINK);
        }
        
        if (!isValidEmailAddress(email)) {
            throwInvalidEmailFormatException(AUTHENTICATE_USER_LINK);
        }
        
        User user = userDataManager.get(email);
        
        if (!(user != null && user.getPassword().equals(MD5.hash(password)))) {
            throwInvalidLoginCredentialsException();
        }        
        
        AppUser appUser = getAppUser(user); 
        appUser = getAppUserWithToken(appUser);        
        
        String smsVerificationCode = appUser.getSmsVerificationCode();
        if (smsVerificationCode == null || !smsVerificationCode.equals("0")) {
            appUser.setSmsVerificationCode("-1");
            appUser.setPhoneNumber("00000000000");
        }        
        
        return appUser;                
    }

    private List<AppRole> getUserAppRoles(String email) {
        List<AppRole> userAppRoles = new ArrayList<AppRole>();
        List<UserRole> userRoles = userRoleDataManager.getByUser(email);
        for (UserRole userRole : userRoles) {
            userAppRoles.add(getAppRole(userRole));
        }
        return userAppRoles;
    }
        
    @Override
    public AppUser addUser(String email, String password, String userType)
            throws NullUserAttributeException, InvalidEmailFormatException, InvalidUserTypeException, 
            DuplicateUserKeyException{

        if (email == null || email.isEmpty() || password == null || password.isEmpty()) {
            throwNullUserAttributeException(ADD_USER_LINK);
        }
        
        if (!isValidEmailAddress(email)) {
            throwInvalidEmailFormatException(ADD_USER_LINK);
        }                

        User user = new User();

        user.setEmail(email);
        user.setFirstName("na");
        user.setLastName("na");
        user.setPhoneNumber("na");
        user.setCreationDate(Calendar.getInstance().getTime());
        user.setPassword(MD5.hash(password));
        user.setIsEmailAddressVerified(false);
        user.setSmsVerificationCode(codeGenerator.getCode());

        AppUser createdUser = null;
        try {
            createdUser = getAppUser(userDataManager.create(user));
        } catch (EJBTransactionRolledbackException etre) {             
            throwDuplicateUserKeyException(ADD_USER_LINK);            
        }
        addUserRole(email, getUserRoleId(userType));
        //newUserPublisher.publishNewUserEvent(user, PUBLISH_TIMEOUT);

        createdUser.setSmsVerificationCode("-1");
        createdUser.setPhoneNumber("00000000000");

        return createdUser;
    }

    private boolean isValidUserType(String userType) {
        for (UserType knownUserType : UserType.values()) {
            if (userType.equals(knownUserType.getDescription())) {
                return true;
            }
        }

        return false;
    }

    private String getUserRoleId(String userType) {
        for (UserType knownUserType : UserType.values()) {
            if (userType.equals(knownUserType.getDescription())) {
                return knownUserType.name();
            }
        }
        return null;
    }
    
    public boolean isValidEmailAddress(String email) {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        Pattern p = Pattern.compile(ePattern);
        Matcher m = p.matcher(email);
        return m.matches();
    }

    @Override
    public Boolean addUserRole(String email, String roleId) {
        if (email != null && roleId != null) {
            UserRole userRole = new UserRole();

            Role role = new Role();
            role.setRoleId(roleId);

            User user = new User();
            user.setEmail(email);

            UserRolePK userRolePK = new UserRolePK();
            userRolePK.setRoleId(roleId);
            userRolePK.setEmail(email);

            userRole.setRole(role);
            userRole.setUser(user);

            userRole.setUserRolePK(userRolePK);

            userRoleDataManager.create(userRole);
            return true;
        }
        return false;
    }

    @Override
    public PlatformCallback verifyEmail(String encodedEmail) 
            throws NullUserAttributeException, UserDoesNotExistException, InternalException {    
        if (encodedEmail == null) {
            throwNullUserAttributeException(VERIFY_USER_EMAIL_LINK);
        }
        
        String email = "";
        try {
            email = new JikaDecode().decode(encodedEmail, 0);
        } catch (ArrayIndexOutOfBoundsException aiobe) {
            throwInternalException(VERIFY_USER_EMAIL_LINK);
        } catch (StringIndexOutOfBoundsException siobe) {
            throwInternalException(VERIFY_USER_EMAIL_LINK);
        }
        
        User user = userDataManager.get(email);
        if (user == null) {
            throwUserDoesNotExistException(VERIFY_USER_EMAIL_LINK);            
        } else if (user.isEmailAddressVerified()) {
            return getCallbackUrl(user.getEmail());
        } else {
            user.setIsEmailAddressVerified(true);
            userDataManager.update(user);
            return getCallbackUrl(user.getEmail());
        }
        PlatformCallback callBack = new PlatformCallback();
        callBack.setCallBackUrl(NO_ROLE_CALLBACK);
        return callBack;
    }

    private PlatformCallback getCallbackUrl(String email) {
        PlatformCallback callBack = new PlatformCallback();
        List<UserRole> userRoles = userRoleDataManager.getByUser(email);
        for (UserRole userRole : userRoles) {
            if (userRole.getUserRolePK().getRoleId().equals(UserType.USER.name())) {
                callBack.setCallBackUrl(USER_CALLBACK);
            } else if (userRole.getUserRolePK().getRoleId().equals(UserType.MENTOR.name())) {
                callBack.setCallBackUrl(MENTOR_CALLBACK);
            } else if (userRole.getUserRolePK().getRoleId().equals(UserType.ADMIN.name())) {
                callBack.setCallBackUrl(ADMIN_CALLBACK);
            }
        }
        return callBack; 
    }

    @Override
    public AppUser verifySmsCode(String email, String smsCode) 
            throws NullUserAttributeException, UserDoesNotExistException {  
        if (email == null || email.isEmpty() || smsCode == null || smsCode.isEmpty()) {
           throwNullUserAttributeException(VERIFY_USER_SMS_CODE_LINK);
        }
        User user = userDataManager.get(email);
        if (user == null) {
            throwUserDoesNotExistException(VERIFY_USER_SMS_CODE_LINK);
        } else {
            if (smsCode.length() > 5 && smsCode.trim().equals(user.getPhoneNumber())) {
                user.setSmsVerificationCode("0");
                userDataManager.update(user);
                return getAppUserWithToken(getAppUser(user));
            } else {
                if (smsCode.trim().equals(user.getSmsVerificationCode())) {
                    user.setSmsVerificationCode("0");
                    userDataManager.update(user);
                    return getAppUserWithToken(getAppUser(user));
                }       
            }
        }
        AppUser appUser = getAppUser(user);
        appUser.setSmsVerificationCode("-1");
        appUser.setPhoneNumber("00000000000");
        return appUser;
    }
    
    
    private AppUser getAppUserWithToken(AppUser appUser) {
        JWT token = new JWT();
        appUser.setUserRoles(getUserAppRoles(appUser.getEmail()));        
        appUser.setAuthToken(token.createJWT(appUser, TOKEN_LIFETIME));
        return appUser;
    }
    
    
    @Override
    public AppUser resendSmsVerification(String email)
            throws NullUserAttributeException, UserDoesNotExistException {

        if (email == null || email.isEmpty()) {
            throwNullUserAttributeException(RESEND_USER_SMS_CODE_LINK);
        }

        User user = userDataManager.get(email);
        if (user == null) {
            throwUserDoesNotExistException(RESEND_USER_SMS_CODE_LINK);
        }
        
        if (!user.getSmsVerificationCode().equals("0")) {
            resendSmsVerificationPublisher.publishResendSmsVerificationEvent(user, PUBLISH_TIMEOUT);        
        }

        AppUser appUser = getAppUser(user);
        appUser.setSmsVerificationCode("-1");
        appUser.setPhoneNumber("00000000000");
        return appUser;
    }
    
    @Override
    public AppUser resendEmailVerification(String email)
            throws NullUserAttributeException, UserDoesNotExistException {

        if (email == null || email.isEmpty()) {
            throwNullUserAttributeException(RESEND_USER_EMAIL_CODE_LINK);
        }

        User user = userDataManager.get(email);
        if (user == null) {
            throwUserDoesNotExistException(RESEND_USER_EMAIL_CODE_LINK);
        }
        
        if (!user.isEmailAddressVerified()) {
            resendEmailVerificationPublisher.publishResendEmailVerificationEvent(user, PUBLISH_TIMEOUT);        
        } 

        AppUser appUser = getAppUser(user);
        appUser.setSmsVerificationCode("-1");
        appUser.setPhoneNumber("00000000000");
        return appUser;
    }
    
    @Override
    public AppUser resetPassword (String email) 
            throws NullUserAttributeException, InvalidEmailFormatException, UserDoesNotExistException {
        if (email == null || email.isEmpty()) {
            throwNullUserAttributeException(RESET_USER_PASSWORD_LINK);
        }
        
        if (!isValidEmailAddress(email)) {
            throwInvalidEmailFormatException(RESET_USER_PASSWORD_LINK);
        }
        
        User user = userDataManager.get(email);
        
        if (user == null ) {
            throwUserDoesNotExistException(RESET_USER_PASSWORD_LINK);
        }    
        
        UserPasswordResetToken token = userPasswordResetTokenManager.create(user.getEmail(), user.getFirstName());
        
        resetPasswordPublisher.publishResetPasswordEvent(token, PUBLISH_TIMEOUT);

        AppUser appUser = getAppUser(user);         
        
        String smsVerificationCode = appUser.getSmsVerificationCode();
        if (smsVerificationCode == null || !smsVerificationCode.equals("0")) {
            appUser.setSmsVerificationCode("-1");
            appUser.setPhoneNumber("00000000000");
        }
        
        return appUser;
    }
    
    @Override
    public AppUser setNewPassword (String email, String password)
            throws NullUserAttributeException, InvalidEmailFormatException {        
        
        if (email == null || email.isEmpty() || password == null || password.isEmpty()) {
            throwNullUserAttributeException(RESET_USER_PASSWORD_LINK);
        }
        
        if (!isValidEmailAddress(email)) {
            throwInvalidEmailFormatException(RESET_USER_PASSWORD_LINK);
        }
        
        User user = userDataManager.get(email);        
        user.setPassword(MD5.hash(password));
        AppUser updateUser = getAppUser(userDataManager.update(user));
        
        return updateUser;
    }
            
    @Override
    public AppUser getUserDetails (String email) 
            throws NullUserAttributeException, InvalidEmailFormatException, UserDoesNotExistException {
        if (email == null || email.isEmpty()) {
            throwNullUserAttributeException(GET_USER_DETAILS);
        }
        
        if (!isValidEmailAddress(email)) {
            throwInvalidEmailFormatException(GET_USER_DETAILS);
        }
        
        User user = userDataManager.get(email);
        
        if (user == null ) {
            throwUserDoesNotExistException(GET_USER_DETAILS);
        }    
        
        return getAppUser(user);
    }        
     
    @Override
    public String authenticatePasswordResetToken(String encodedEmail, String token) 
            throws NullUserAttributeException, UserDoesNotExistException {
        if (encodedEmail == null || encodedEmail.isEmpty() || token == null || token.isEmpty()) {
            throwNullUserAttributeException(PASSWORD_RESET_TOKEN_LINK);
        }
        
        String email = new JikaDecode().decode(encodedEmail, 0);
        User user = userDataManager.get(email);
        
        if (user == null ) {
            throwUserDoesNotExistException(PASSWORD_RESET_TOKEN_LINK);
        }
        
        UserPasswordResetToken userToken = userPasswordResetTokenManager.get(email);
        
        if (userToken == null) {
            return Boolean.FALSE.toString();
        }
        
        Date now = Calendar.getInstance().getTime();
        long milisecondsLeft = (userToken.getExpiryTime().getTime() + 14400) - now.getTime();
        if (milisecondsLeft < 0) { 
            //throw password expired exception
            return Boolean.FALSE.toString();
        }
        if (!token.equals(userToken.getToken())) {
            //throw invalid token exception
            return Boolean.FALSE.toString();
        }
        
        userPasswordResetTokenManager.delete(email);
        
        return Boolean.TRUE.toString();
    }
    
    private void throwNullUserAttributeException(String link) throws NullUserAttributeException {
        throw new NullUserAttributeException(Response.Status.BAD_REQUEST.getStatusCode(),
                    400, INCOMPLETE_DATA_ERROR, INCOMPLETE_DATA_ERROR_DETAILS, link);
    }
    
    private void throwInternalException(String link) throws InternalException {
        throw new InternalException(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode(),
                    500, INTERNAL_SERVER_ERROR, INTERNAL_SERVER_ERROR_DETAILS, link);
    }    
    
    private void throwUserDoesNotExistException(String link) throws UserDoesNotExistException {
        throw new UserDoesNotExistException(Response.Status.NOT_FOUND.getStatusCode(),
                    404, USER_NOT_FOUND_ERROR, USER_NOT_FOUND_ERROR_DETAILS, link);
    }
    
    private void throwInvalidUserTypeException(String link) throws InvalidUserTypeException {
        throw new InvalidUserTypeException(Response.Status.BAD_REQUEST.getStatusCode(),
                400, INVALID_USER_TYPE_ERROR, INVALID_USER_TYPE_ERROR_DETAILS,
                link);
    }
    
    private void throwDuplicateUserKeyException(String link) throws DuplicateUserKeyException {
        throw new DuplicateUserKeyException(Response.Status.BAD_REQUEST.getStatusCode(),
                    400, USER_ALREADY_EXISTS_ERROR, USER_ALREADY_EXISTS_ERROR_DETAILS, link);
    }

    @Override
    public boolean removeDonCasino() {
        userRoleDataManager.delete(userRoleDataManager.getByUser("doncasino14@gmail.com").get(0));
        userDataManager.delete(userDataManager.get("doncasino14@gmail.com"));
        return true;
    }

    @Override
    public boolean removeSadiq() {
        userRoleDataManager.delete(userRoleDataManager.getByUser("abubakarhassan59@gmail.com").get(0));
        userDataManager.delete(userDataManager.get("abubakarhassan59@gmail.com"));
        return true;
    }

    @Override
    public boolean removeLattie() {
        userRoleDataManager.delete(userRoleDataManager.getByUser("queenlattie720@gmail.com").get(0));
        userDataManager.delete(userDataManager.get("queenlattie720@gmail.com"));
        return true;
    }
    
    private void throwInvalidLoginCredentialsException() throws InvalidLoginCredentialsException {
        throw new InvalidLoginCredentialsException(Response.Status.UNAUTHORIZED.getStatusCode(),
                401, INVALID_LOGIN_CREDENTIALS_ERROR, INVALID_LOGIN_CREDENTIALS_ERROR_DETAILS,
                AUTHENTICATE_USER_LINK);
    }
    
    private void throwInvalidEmailFormatException(String link) throws InvalidEmailFormatException {
        throw new InvalidEmailFormatException(Response.Status.BAD_REQUEST.getStatusCode(),
                400, INVALID_EMAIL_FORMAT_ERROR, INVALID_EMAIL_FORMAT_ERROR_DETAILS,
                link);
    }    
}
