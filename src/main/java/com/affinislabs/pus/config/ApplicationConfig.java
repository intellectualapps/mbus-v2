/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.affinislabs.pus.config;

import com.affinislabs.pus.service.UserService;
import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.core.Application;
import com.affinislabs.pus.util.exception.DuplicateUserKeyExceptionMapper;
import com.affinislabs.pus.util.exception.InternalExceptionMapper;
import com.affinislabs.pus.util.exception.InvalidEmailFormatExceptionMapper;
import com.affinislabs.pus.util.exception.InvalidUserTypeExceptionMapper;
import com.affinislabs.pus.util.exception.NullUserAttributeExceptionMapper;
import com.affinislabs.pus.util.exception.UserDoesNotExistExceptionMapper;
import com.affinislabs.pus.util.exception.InvalidLoginCredentialsExceptionMapper;

/**
 *
 * @author Lateefah
 */
@javax.ws.rs.ApplicationPath("/api")

public class ApplicationConfig extends Application{
    @Override    
    public Set<Class<?>> getClasses() {
        Set<Class<?>> s = new HashSet<Class<?>>();
        s.add(UserService.class);
        s.add(NullUserAttributeExceptionMapper.class);
        s.add(InvalidUserTypeExceptionMapper.class);
        s.add(UserDoesNotExistExceptionMapper.class);
        s.add(DuplicateUserKeyExceptionMapper.class);
        s.add(InvalidLoginCredentialsExceptionMapper.class);
        s.add(InvalidEmailFormatExceptionMapper.class);
        return s;
    }
}
